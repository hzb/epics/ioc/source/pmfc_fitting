## Fitting for a PFMC Camera system at EMIL

This IOC monitors values from a given IOC stats waveform and fits a gaussian to it

## How to run the IOC in the container?

for example:
```
docker run -it -d --name=sissy1_fitting --network=host --restart always -e PV_PREFIX=SISSY1EX:PMFC00 pmfc_fitting
```
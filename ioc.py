import epics
import numpy as np
import threading
import sys
import time

import numpy as np
from scipy.optimize import curve_fit
import time

def gfit(ydata, outdata):
    ## constants
    DEBUG=False
    SNR_threshold = 1.0
    FWHM_factor = 2.355

    t1=time.time()

    ## Gaussian equation with linear background
    def func(x, a, b, c, d, e):
        return a*x + b + c * np.exp(-(np.power((x - d), 2) / (2 * np.power(e, 2))))

    ## some parameters for fitting
    ydim=len(ydata)
    xdata = np.linspace(0,ydim-1,ydim)
    bg_slope = 0
    bg_value = np.min(ydata)
    height = np.max(ydata)
    center = np.argmax(ydata)

    snr = 100*((height/np.mean(ydata))-1)
    outdata["snr"]=snr
    if DEBUG: print('snr: {:.1f} %'.format(snr))

    ## try to identified a valid profile
    if SNR_threshold>snr:
        outdata["valid"]=False
        return

    ## more parameters for fitting
    ycut=(height-bg_value)/2+bg_value
    ycutmask=(ydata>ycut)
    y1=np.multiply(ydata,ycutmask)
    y2=np.diff(y1)
    sigma_guess=np.abs((np.argmin(y2)-np.argmax(y2))/2)
    p0=[bg_slope, bg_value, height, center, sigma_guess]

    ## fitting
    popt, pcov = curve_fit(func, xdata, ydata, p0=p0)

    ## center of mass calc
    center = popt[3]
    sigma = popt[4]
    # limit domain to 3.sigma to each side of the gaussian center
    #x0 = int(np.max([0, np.floor(center-3*sigma)]))
    #x1 = int(np.min([ydim-1, np.ceil(center+3*sigma)]))
    ycm = ydata#[x0:x1]
    xcm = xdata#[x0:x1]
    ycm = np.subtract(ycm, np.min(ycm))
    cmass = np.sum(np.multiply(xcm,ycm))/np.sum(ycm)
    cmassdiff = cmass-center
    if DEBUG: print("Cmass_dif: {:.9f}".format(cmassdiff))

    ## Return values
    outdata["valid"]=True
    outdata["center"] = center
    outdata["center_m"] = cmass
    outdata["fwhm"] = sigma*FWHM_factor
    outdata["fitarray"] = func(xdata, *popt)
    outdata["cmass_diff"] = cmassdiff
    outdata["residual"] = ydata -func(xdata, *popt)
    t2=time.time()
    if DEBUG: print('dt: {:.1f} ms\n'.format((t2-t1)*1000.0))
    return

def main():
    if len(sys.argv)>2:
        N = sys.argv[1]
        PG = sys.argv[2]
    else:
        print("   Please add arguments.")
        print("   > python3 bpmmon {BPM number} {Camera}")
        print("   Ex:")
        print("      $ python3 bpmmon.py 1 PG01")
        return

    ## constants
    DEBUG=False

    ## Loop lock to sync with PV data
    global eve
    eve = threading.Event()

    ## epics PVs
    profileX_pvname = "{}:Stats2:ProfileAverageX_RBV".format(PG)
    profileY_pvname = "{}:Stats2:ProfileAverageY_RBV".format(PG)
    centerX_pvname = "{}:BPM:{}:X:center".format(PG,N)
    centerY_pvname = "{}:BPM:{}:Y:center".format(PG,N)
    centerX_mass_pvname = "{}:BPM:{}:X:center_mass".format(PG,N)
    centerY_mass_pvname = "{}:BPM:{}:Y:center_mass".format(PG,N)
    valid_pvname = "{}:BPM:{}:valid".format(PG,N)
    enable_pvname = "{}:BPM:{}:enable".format(PG,N)
    plot_enable_pvname = "{}:BPM:{}:plot_enable".format(PG,N)
    fwhmX_pvname = "{}:BPM:{}:X:fwhm".format(PG,N)
    fwhmY_pvname = "{}:BPM:{}:Y:fwhm".format(PG,N)
    fitarrayX_pvname = "{}:BPM:{}:X:ydata".format(PG,N)
    fitarrayY_pvname = "{}:BPM:{}:Y:ydata".format(PG,N)
    roiposX_pvname = "{}:ROI1:MinX_RBV".format(PG)
    roiposY_pvname = "{}:ROI1:MinY_RBV".format(PG)
    resolutionY_pvname = "{}:BPM:{}:Y:resolution".format(PG,N)
    resolutionX_pvname = "{}:BPM:{}:X:resolution".format(PG,N)
    angleY_pvname = "{}:BPM:{}:Y:angle_correction".format(PG,N)
    angleX_pvname = "{}:BPM:{}:X:angle_correction".format(PG,N)
    centerX_mass_diff_pvname = "{}:BPM:{}:X:cmass_diff".format(PG,N)
    centerY_mass_diff_pvname = "{}:BPM:{}:Y:cmass_diff".format(PG,N)
    residualX_pvname = "{}:BPM:{}:X:residual".format(PG,N)
    residualY_pvname = "{}:BPM:{}:Y:residual".format(PG,N)


    ## callback function
    def onValueChange(pvname=None, value=None, char_value=None, **kw):
        global eve
        eve.set()

    ## create PV channels
    profileX = epics.PV(profileX_pvname, auto_monitor=True, callback=onValueChange)
    profileY = epics.PV(profileY_pvname, auto_monitor=True)
    centerX = epics.PV(centerX_pvname, auto_monitor=False)
    centerY = epics.PV(centerY_pvname, auto_monitor=False)
    centerX_mass = epics.PV(centerX_mass_pvname, auto_monitor=False)
    centerY_mass = epics.PV(centerY_mass_pvname, auto_monitor=False)
    valid = epics.PV(valid_pvname, auto_monitor=False)
    enable = epics.PV(enable_pvname, auto_monitor=True)
    plot_enable = epics.PV(plot_enable_pvname, auto_monitor=True)
    fwhmX = epics.PV(fwhmX_pvname, auto_monitor=False)
    fwhmY = epics.PV(fwhmY_pvname, auto_monitor=False)
    fitarrayX = epics.PV(fitarrayX_pvname, auto_monitor=False)
    fitarrayY = epics.PV(fitarrayY_pvname, auto_monitor=False)
    roiposX = epics.PV(roiposX_pvname, auto_monitor=True)
    roiposY = epics.PV(roiposY_pvname, auto_monitor=True)
    resolutionY = epics.PV(resolutionY_pvname, auto_monitor=True)
    resolutionX = epics.PV(resolutionX_pvname, auto_monitor=True)
    angleY = 1
    angleX = 1
    centerX_mass_diff = epics.PV(centerX_mass_diff_pvname, auto_monitor=False)
    centerY_mass_diff = epics.PV(centerY_mass_diff_pvname, auto_monitor=False)
    residualX = epics.PV(residualX_pvname, auto_monitor=False)
    residualY = epics.PV(residualY_pvname, auto_monitor=False)
    ## dictionary structure
    Xfit = {
        "valid" : False,
        "snr" : 0.0,
        "center" : 0,
        "center_m" : 0,
        "cmass_diff" : 0,
        "fwhm" : 0.0,
        "fitarray" : [],
        "residual" : []
    }
    Yfit = Xfit.copy()

    ## Main loop
    print("BPM Monitor has started:")
    print("BPM{} with {}".format(N,PG))

    while(True):
        try:
            #print("Waiting")
            eve.wait()
            t1=time.time()
            #print("Fitting")
            if enable.value>0:
                print("Fitting")
                gfit(profileX.value, Xfit)
                gfit(profileY.value, Yfit)
                isvalid = Xfit["valid"]
                valid.put(isvalid)
                if isvalid:
                    centerX.put(Xfit["center"]+roiposX.value)
                    centerY.put(Yfit["center"]+roiposY.value)
                    centerX_mass.put(Xfit["center_m"]+roiposX.value)
                    centerY_mass.put(Yfit["center_m"]+roiposY.value)
                    fwhmX.put(Xfit["fwhm"]*resolutionX.value)
                    fwhmY.put(Yfit["fwhm"]*resolutionY.value)
                    centerX_mass_diff.put(Xfit["cmass_diff"]*resolutionX.value)
                    centerY_mass_diff.put(Yfit["cmass_diff"]*resolutionY.value)
                    if plot_enable.value>0:
                        fitarrayX.put(Xfit["fitarray"])
                        fitarrayY.put(Yfit["fitarray"])
                        residualX.put(Xfit["residual"])
                        residualY.put(Yfit["residual"])
                else:
                    print("Not Valid")
            else:
                valid.put(0)
            t2=time.time()
            if DEBUG: print('dt: {:.3f} ms\n'.format((t2-t1)*1000.0))
        except Exception as err:
            print("[{}] Catched exception:".format(time.asctime()))
            print(err)
        eve.clear()

main()
print("OK")
